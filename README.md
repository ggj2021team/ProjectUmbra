# **aVoid**

Game project submission for Global Game Jam 2021

_After a catastrophic event leaves you stranded in space surrounded by debris in the darkness, you will need to use your head to navigate the hazards of the wreckage and find your way to a comms terminal to call for help._

## **_The gameplay is really simple_**

-The character can look around and move in the direction they are looking at, but there's a catch. You can only move by using limited propulsion bursts that are enough to sen you floating through endless space to your eventual demise, so be careful. Use the environment around you to plan your escape route. Look out for burst refills, they are essential to help you on your journey. Beware of harmful debris around you, as they can bring you ever closer to your doom._

## **Credits:** 
**Caio Souza Fonseca(dertzack): Programming Generalist**

**Igor Antonio Pereira Machado(IgorAPM): Programming/Sound Design**

**Rodrigo Villani(RVillani): Technical Artist**

**Tiago dos Santos Patrocínio(tiagodsp): Programming Generalist**
